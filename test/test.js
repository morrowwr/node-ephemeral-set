
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// for managing async things
const EventEmitter = require('events');
const emitter = new EventEmitter();

// for generating random keys
const _crypto = require( 'crypto' );

const getKey = () => {
	return _crypto.randomBytes(12).toString('hex');
}

// get a random integer between 1 and n
const _randint = ( n ) => ( Math.floor( Math.random() * parseInt(n) ) + 1 );

// do something n times
const _times = ( f , n ) => { for( let i = 0 ; i < parseInt(n) ; i++ ) { f(); } };

// get "now" in seconds
const _seconds = () => ( Date.now() / 1000.0 );

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * 
 * SETUP EPHEMERAL SET AND GLOBAL ACTIONS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// a simple on expire function, that (more or less) just says "expiring..."
const onExp = ( k ) => { console.log( "expiring " + k + " at " + _seconds() ); };

// get actual EphemeralSet class
const _ES = require( __dirname + '/../src/index.js' );

// create ephemeral set
const es = new _ES( onExp , 'T5S' , 'T2S' );

// print when things happen
es.on( 'check' , () => { console.log( `Checking expirations at ${_seconds()}` ); } );
es.on( 'start' , () => { console.log( `EphemeralSet starting check loop at ${_seconds()}` ); } );
es.on( 'stop'  , () => { console.log( `EphemeralSet stopping check loop at ${_seconds()}` ); } );

// test index and tests array
let ti = 0 , tests = [];

// 
emitter.on( 'ready' , () => {
	es.print();
	es.start();
} );

// 
emitter.on( 'empty' , () => {
	ti += 1; 
	if( ti < tests.length ) { tests[ti](); }
	else { 
		console.log( "\n\n" );
		process.exit(); 
	}
} );

// 
es.on( 'empty' , () => { 
	console.log( `EphemeralSet is empty at ${_seconds()}` );
	es.stop(); // an "autostop" implementation
	emitter.emit( 'empty' );
} );

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * 
 * DEFINE THE TESTS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// trivial test
tests.push(
	() => {

		console.log( "\nTrivial test: add and expire\n" );

		es.start();
		es.add( "object 1" ); 
		es.print();
	}
);

// add some data to it, starting the timer when that is done
tests.push(
	() => {

		console.log( "\nTesting ordered expire times when adding\n" );

		let count = 0 , max = _randint(10);
		const addData = () => {
			if( count >= max ) { return emitter.emit( 'ready' ); }
			setTimeout( () => {
				_times( () => { es.add( getKey() ); } , _randint( 10 ) );
				count += 1;
				addData();
			} , 1000 );
		};
		addData();
	}
);

// add some data with scattered lifetimes
tests.push(
	() => {

		console.log( "\nTesting unordered expire times when adding\n" );

		let count = 0 , max = _randint(10);
		const addData = () => {
			if( count >= max ) { 
				console.log( '- - - - - - - - - - - - - - - - ' ); 
				return emitter.emit( 'ready' ); 
			}
			setTimeout( () => {
				_times( () => { es.add( getKey() , _randint(10) ); } , _randint( 10 ) );

				// to see the linked list get built
				console.log( '- - - - - - - - - - - - - - - - ' ); 
				es.print(); 

				count += 1;
				addData();
			} , 1000 );
		}
		addData();
	}
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * 
 * RUN TESTS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// start the testing event loop by calling the first test
tests[0]();

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// wait for input that won't arrive
process.stdin.resume();