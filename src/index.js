
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * DEFAULTS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// defaults
const defaultLifeSecs  = 60.0;
const defaultDelaySecs = 10.0;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * LOCAL UTILITIES
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// a regular expression to recognize floats, including scientific notation
const floatRegex = /^[+-]{0,1}(([0-9]+){0,1}(\.[0-9]+){0,1})([eE][+-]{0,1}(([0-9]+){0,1}(\.[0-9]+){0,1})){0,1}$/;

// ISO 8601 Durations, of the form
// 
//      P[n]Y[n]M[n]DT[n]H[n]M[n]S
// 
// We use a rough RegEx
const iso8601dur = /^(P([0-9]+[YMWD]){0,4}){0,1}(T([0-9]+[HMS]){0,3}){0,1}$/i;

// conversions for each time unit into seconds
var units = { TS : 1.0 };
units.TM = units.TS *  60.0; // 60 seconds in a minute
units.TH = units.TM *  60.0; // 60 minutes in an hour
units.PD = units.TH *  24.0; // 24 hours in a day
units.PW = units.PD *   7.0; // 7 days a week
units.PY = units.PD * 365.0; // 365 days a year
units.PM = units.PY /  12.0; // 12 months in a year (approximate)

// convert an ISO8601 duration string to seconds
const iso8601Secs = ( s ) => {

    let match = iso8601dur.exec( s );
    if( ! match ) { return null; }

    let p = match[1] , t = match[4] , secs = 0.0;

    if( p ) {
        let pre = /([0-9]+)([YMWD])/ig , m;
        while( ( m = pre.exec( p ) ) !== null ) {
            secs += parseInt( m[1] ) * units[ 'P' + m[2].toUpperCase() ];
        }
    }

    if( t ) {
        let tre = /([0-9]+)([HMS])/ig , m;
        while( ( m = tre.exec( t ) ) !== null ) {
            secs += parseInt( m[1] ) * units[ 'T' + m[2].toUpperCase() ];
        }
    }

    return secs;

}

// a wrapper to get "now" in seconds
const seconds = () => ( Math.floor( Date.now() / 1000.0 ) );

// parse a value as "Seconds", presuming the value is a float or is 
// an ISO 8601 duration string. return null if neither. 
const parseSeconds = ( s ) => {
    let res = null;
    if( floatRegex.test( `${s}` ) ) { 
        res = parseFloat( s );
    } else if( iso8601dur.test( s ) ) {
        res = iso8601Secs( s );
    }
    return res;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * EPHEMERAL SET CLASS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = class EphemeralSet {

    // constructor( defaults )
    constructor( onExpire , life , delay ) {

        this.lists   = { };
        this.first   = null;
        this.last    = null;
        this.running = false;
        this.timer   = null;
        this.events  = {
            'empty'  : [] , // e.g., stop(), don't loop while empty
            'start'  : [] , 
            'stop'   : [] , 
            'add'    : [] , // e.g., start(), to autostart expirations
            'check'  : [] , 
            'expire' : [ onExpire ] , // thing(s) to do when expiring?
        }

        this.useInterval = true;

        this.life = parseSeconds( life );
        if( this.life === null ) { 
            this.life = defaultLifeSecs; 
        }

        this.delay = parseSeconds( delay );
        if( this.delay === null ) { 
            this.delay = defaultDelaySecs; 
        }

    }

    // add a key, with a particular lifetime
    // 
    // TBD: allow object data, and custom expire callback(s), too?
    // 
    add( key , life ) {

        // parse any passed "life" parameter
        let exp = parseSeconds( life );
        if( exp === null ) { exp = this.life; }
        exp += seconds(); // add "life" to current seconds

        // get a string version of the expire time for a linked-list key
        let expKey = `${exp}`;

        if( expKey in this.lists ) { 

            // change an _existing_ part of the list

            this.lists[ expKey ].list.push( key ); 

        } else { 

            // entirely new entry for the lists store... 

            // create a new lists element (with no "prev" or "next", _yet_)
            this.lists[ expKey ] = { list : [ key ] };

            // if there is no "first" node yet, this is it
            if( ! this.first ) { this.first = expKey; }

            // because we use time to key, we always shift "up" when creating a new node
            // make sure we tell the last node to point to "now" as "next", if there is one
            if( ! this.last ) { this.last = expKey; }
            else { // this.last must exist

                if( exp > parseFloat( this.last ) ) { // the new key is after the current last
                    this.lists[ this.last ].next = expKey; // current last preceeds new key
                    this.lists[ expKey ].prev = this.last; // new key succeeds current last
                    this.last = expKey; // reset the last key to the new key
                } else {
                    let next = this.last; // start at the end of the linked list
                    while( exp < parseFloat( next ) ) { // we will have at least one iteration
                        if( ! this.lists[ next ].prev ) { // if we can't step back...
                            // there IS NO previous element... we're at first entry
                            this.first = expKey; // new key will be first
                            break; // leave loop
                        }
                        next = this.lists[ next ].prev; // move along list "backwards"
                    }
                    this.lists[ next ].prev = expKey; // new key preceeds the "next" key
                    this.lists[ expKey ].next = next; // "next" key succeeds the new key
                }

            }

        } // finished creating new entry

    }

    // remove a key from the list of things we're tracking
    remove( key ) {

        if( ! this.first ) { return; }

        let curr = this.first;
        while( curr ) {
            if( this.lists[curr].list.indexOf( key ) < 0 ) {
                curr = this.lists[curr].next;
            } else {
                this.lists[curr].list = this.lists[curr].list.filter( k => ( k != key ) );
            }
        }

    }

    // event-emitter style on(event) call function routine
    //
    // TBD: how would we remove such "listeners"?
    // 
    on( e , f ) {
        if( e in this.events ) {
            this.events[e].push( f );
        }
    }

    // print status of current list
    print() {

        if( ! this.first ) {
            return console.log( "EphemeralSet: Add objects to print." );
        }

        console.log( "EphemeralSet: printing contents..." );
        let curr = this.first;
        while( curr ) {
            console.log( `  expire time: ${curr}` );
            console.log( `  keys: ` );
            this.lists[curr].list.forEach( i => console.log(`    ${i}`) );
            curr = this.lists[curr].next;
        }

    }

    // clear( expire: truthy/falsey )
    clear( expire ) {

        // do we actively "expire" all the objects stored before clearing them?
        if( expire ) {

            while( this.first ) {

                // call the passed "onExpire" for all the objects in the list
                this.lists[this.first].list.forEach( k => {
                    this.expire(k);
                } );

                // get the next item
                this.first = this.lists[this.first].next;

            }

        }

        // clear internal data
        this.lists   = { };
        this.first   = null;
        this.last    = null;
        this.running = false;
        this.timer   = null;

    }

    // actually expire an object stored with key, maybe with custom maps
    expire( key , maps ) {
        if( maps ) {
            maps.forEach( f => { f(key); } );
        } else {
            this.events.expire.forEach( f => { f(key); } );
        }
    }

    // check to see if we need to expire objects
    check( ) {

        // if there isn't a "first", there isn't a list of lists. we can ignore. 
        if( ! this.first ) { return; }

        // if we have on(expire) callbacks, execute them
        this.events.check.forEach( f => f() );

        // until we get to a key after "now" (which we can evaluate with each loop 
        // invocation) expire associated elements
        while( parseFloat( this.first ) <= seconds() ) {

            // store the first key and, if "first" is null, stop checking
            let key = this.first; if( ! key ) { break; }

            // move the stored reference to the "first" "forward"
            this.first = this.lists[key].next; 

            // call the passed "onExpire" for all the objects in the list
            this.lists[key].list.forEach( k => { 
                this.expire(k); // only executing defaults here?
                // list ~ { ... }, with list[k].data? list[k].expire?
            } );

            // delete the list from the lists (although it doesn't really matter)
            delete this.lists[key];

        }

        // do we need to delete "last" too? only if "first" is null
        if( ! this.first ) { 

            // if list is empty, last is null too
            this.last = null;

            // if we have on(empty) callbacks, execute them
            this.events.empty.forEach( f => f() );

        }

    }

    // a function we can call recursively to use timeout-based checks
    _delayedCheck( delay ) {
        this.timer = setTimeout( () => {
            this.check();
            clearTimeout( this.timer );
            this._delayedCheck( delay );
        } , delay );
    }

    // start an expire checker interval
    start( delay ) {
        
        // "start" only if "stopped"
        if( this.running ) { return; }

        // if we have on(start) callbacks, execute them
        this.events.start.forEach( f => f() );

        // get the potentially customized delay
        let del = parseSeconds( delay );
        if( del === null ) { del = this.delay; }
        del *= 1000.0; // ms for interval call

        // start the check loop
        if( this.useInterval ) {
            this.timer = setInterval( () => { this.check(); } , del );
        } else {
            this._delayedCheck( del );
        }

        // set the running flag
        this.running = true;

    }

    // re-start an expire checker interval
    restart( delay ) { this.stop(); this.start(); }

    // stop an expire checker interval
    stop() {

        if( this.running ) { 

            // clear the interval or timeout being used
            if( this.useInterval ) { clearInterval( this.timer ); }
            else { clearTimeout( this.timer ); }

            // if we have on(stop) callbacks, execute them
            this.events.stop.forEach( f => f() );

        }

        // clear running flag
        this.running = false;
    }

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Copyright 2019+, W. Ross Morrow and Stanford University
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */